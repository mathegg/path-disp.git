@Main
{
    @Envs()
    {
        @BindRoot($.msg)($.input.0)
        @BindRoot($.publish)($.input.1)
    }
    
    @Invalid($.msg)
    {
        @BindRoot($.msg)("小改动")
    }
        
    @Println("正在准备上传到仓库")
     // Git to github
    @Command("git add .")
    @Command("git commit -m $.msg")
    @Command("git push origin master")
    @Println("成功上传到仓库")
    @Println("") 


    @Valid($.publish)
    {
        
        @File("E:/Rust/.cargo/token.txt")
        {
            @Bind($.url)("https://github.com/rust-lang/crates.io-index")
            @Command("cargo publish --index $.url --token $.content")
            @Println("发布成功")
            @Println("")
        } 
    }
}